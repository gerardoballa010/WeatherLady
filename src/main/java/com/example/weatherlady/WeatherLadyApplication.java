package com.example.weatherlady;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherLadyApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherLadyApplication.class, args);
    }

}
